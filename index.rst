.. S-AiMS documentation master file, created by Jeremiah of GoodhopeDesigns & Informatics
   sphinx-quickstart on Tue Apr  8 19:04:43 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to S-AiMS's documentation!
==================================

Introduction
==================================
.. toctree::
   :maxdepth: 2

Functionalities
==================================

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

